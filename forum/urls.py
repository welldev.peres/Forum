from django.urls import path
from . import views

urlpatterns = [
    path('', views.category_list, name='category_list'),
    path('category/<int:category_id>/', views.topic_list, name='topic_list'),
    path('topic/<int:topic_id>/', views.topic_detail, name='topic_detail'),
    path('category/<int:category_id>/create/', views.create_topic, name='create_topic'),
    path('topic/<int:topic_id>/reply/', views.create_post, name='create_post'),
]
